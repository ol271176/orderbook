package com.company;

public class Ask extends Order {

    public Ask(long price, long size) {
        super(price, size);
        OrderBook.getAsks().add(this);
    }
}
