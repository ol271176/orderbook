package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class OrderBook {

    private static Set<Bid> bids = new TreeSet<>(Comparator.comparingLong(Order::getPrice).reversed());
    private static Set<Ask> asks = new TreeSet<>(Comparator.comparingLong(Order::getPrice));

    public static String getBestBid() {
        Iterator<Bid> iterator = bids.iterator();
        while (iterator.hasNext()) {
            Bid bid = iterator.next();
            return bid.toString();
        }
        return null;
    }

    public static String getBestAsk() {
        Iterator<Ask> iterator = asks.iterator();
        while (iterator.hasNext()) {
            Ask ask = iterator.next();
            return ask.toString();
        }
        return null;
    }

    public static String size(long price) {
        StringBuilder str = new StringBuilder("");
        if (asks.size() > 0) {
            Optional<Ask> ask = asks.stream().filter(x -> x.getPrice() == price).findFirst();
            if (!ask.isEmpty()) {
                str.append(String.valueOf(ask.get().getSize() + "\n"));
            }
        }
        if (bids.size() > 0) {
            Optional<Bid> bid= bids.stream().filter(x -> x.getPrice() == price).findFirst();
            if (!bid.isEmpty()){
                str.append(String.valueOf(bid.get().getSize() + "\n"));
            }
        }
        if(str.length() == 0) return String.valueOf(0);
        return str.toString();
    }

    public static void buy(long n) {
        Optional<Long> optional = asks.stream().map(x -> x.getSize()).reduce((x, y) -> x + y);
        long sum = optional.get();
        if (sum > n) {
            Iterator<Ask> iterator = asks.iterator();
            while (iterator.hasNext()) {
                Ask ask = iterator.next();
                if (n < ask.getSize()) {
                    ask.setSize(ask.getSize() - n);
                    break;
                } else {
                    n = n - ask.getSize();
                    ask.setSize(0);
                    continue;
                }
            }
        }
    }

    public static void sell(long n) {
        Optional<Long> optional = bids.stream().map(x -> x.getSize()).reduce((x, y) -> x + y);
        long sum = optional.get();
        if (sum > n) {
            Iterator<Bid> iterator = bids.iterator();
            while (iterator.hasNext()) {
                Bid bid = iterator.next();
                if (n < bid.getSize()) {
                    bid.setSize(bid.getSize() - n);
                    break;
                } else {
                    n = n - bid.getSize();
                    bid.setSize(0);
                    continue;
                }
            }
        }
    }

    public static Set<Bid> getBids() {
        return bids;
    }

    public static Set<Ask> getAsks() {
        return asks;
    }

    public static void output() {
        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
             FileWriter fileWriter = new FileWriter("output.txt")) {
            String s;
            while ((s = reader.readLine()) != null) {
                String[] line = s.split(",");
                if (line[0].equals("u")) {
                    if (line[3].equals("bid")) {
                        Order bid = new Bid(Long.parseLong(line[1]), Long.parseLong(line[2]));
                    }
                    if (line[3].equals("ask")) {
                        Order ask = new Ask(Long.parseLong(line[1]), Long.parseLong(line[2]));
                    }
                }

                if (line[0].equals("o")) {
                    if (line[1].equals("buy")) {
                        buy(Long.parseLong(line[2]));
                    }
                    if (line[1].equals("sell")) {
                        sell(Long.parseLong(line[2]));
                    }
                }

                if (line[0].equals("q")) {
                    if (line[1].equals("best_bid")) {
                        fileWriter.write(getBestBid() + "\n");
                        fileWriter.flush();
                    }
                    if (line[1].equals("best_ask")) {
                        fileWriter.write(getBestAsk() + "\n");
                        fileWriter.flush();
                    }
                    if (line[1].equals("size")) {
                        fileWriter.write(size(Long.parseLong(line[2])));
                        fileWriter.flush();
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
