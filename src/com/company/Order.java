package com.company;

public class Order {
    private long price;
    private long size;

    public Order(long pprice, long ssize) {
        if (pprice > 0) {
            price = pprice;
        } else {
            throw new IllegalArgumentException();
        }
        if (ssize >= 0) {
            size = ssize;
        } else {
            throw new IllegalArgumentException();
        }

    }

    public long getPrice() {
        return price;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String toString() {
        return price + "," + size;
    }

}
