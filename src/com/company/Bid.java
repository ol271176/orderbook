package com.company;

public class Bid extends Order {

    public Bid(long price, long size) {
        super(price, size);
        OrderBook.getBids().add(this);
    }
}
